
<p align="center" width="100%">
  
<img width="100%" height="50%"  src="https://github.com/andresWeitzel/Graphics/blob/main/Gifs/serverraum-v02.gif" />

 </p>

# <img width="50" height="40"  src="https://github.com/andresWeitzel/Graphics/blob/master/Gifs/fb4dbe3a8f97a5c52568a23dd97d15-unscreen.gif" /> Mi Perfil  

* Desarrollador Web Java Full Stack.
* Apasionado por la  Programación, Informática, Robótica, Telecomunicaciones, Electrónica y Otras Áreas.
* Mi primer programa fue en C programando el tipo de Microcontrolador PIC16F84A.
* Me gustaría poder contribuir, en un futuro cercano, en el área de la Inteligencia Artifical y Microelectrónica para ayudar a prevenir y curar enfermedades crónicas en Animales y Seres Humanos
 * Contribuyo y Apoyo al Movimiento del Software Libre.

</br>

<div style="display: inline-block;">
  
![Metrics](https://github.com/andresWeitzel/andresWeitzel/blob/main/github-metrics.svg)

</div>


</br>


<div style="display: inline-block;">
  
![Full-Year-Calendar](https://github.com/andresWeitzel/andresWeitzel/blob/main/metrics.plugin.isocalendar.svg)
  
</div>




</br>



<div style="display: inline-block;">
  
![Activities-Charts](https://github.com/andresWeitzel/andresWeitzel/blob/main/metrics.plugin.habits.charts.svg)

</div>








</br>



<div style="display: inline-block;">
  
![Metrics](https://github.com/andresWeitzel/andresWeitzel/blob/main/metrics.plugin.licenses.ratio.svg)

</div>






  
</br>

---
## Lenguajes y Tecnologías

<div style="display: inline-block;">
  
![Most-Used-languages](https://github.com/andresWeitzel/andresWeitzel/blob/main/metrics.plugin.languages.details.svg)
  
</div>

</br>

* Frontend Stack

<div style="display: inline-block;">
  <img width="40" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/front/html5.png" />
  <img width="40" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/front/css3.png" />
  <img width="42" height="44" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/front/scss.png" />
  <img width="35" height="36" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/front/bootstrap.png" />
  <img width="45" height="37" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/front/js.png" />
  <img width="37" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/front/jquery.png" />
  <img width="40" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/front/angular.png" />
  <img width="45" height="45" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/front/vsc.png" />
  <img width="35" height="35" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/front/thymeleaf.png" />
  <img width="34" height="34" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/front/sublime-text.jpg" />
 
</div>

</br>

* Backend Stack

<div style="display: inline-block;">
  <img width="45" height="45" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/java.png" />
  <img width="40" height="38" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/spring/spring-original.svg" />
  <img width="40" height="38" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/hibernate.png" />
   <img width="45" height="44" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/jsf.png" />
  <img width="45" height="44" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/maven.png" />
  <img width="44" height="44" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/postman.png" />
  <img width="47" height="50" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/lombok.png" />
  <img width="44" height="44" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/swagger.png" />
  <img width="40" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/eclipse.png" />
  <img width="44" height="44" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/netbeans.png" />
  <img width="44" height="44" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/sts.png" />
   <img width="44" height="44" src="https://cdn.icon-icons.com/icons2/2367/PNG/512/terminal_shell_icon_143501.png" />
  <img width="44" height="44" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/git.png" />
  
  
  
</div>

</br>

* Database Stack

<div style="display: inline-block;">
    <img width="41" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/database/oracle.png" /> 
   <img width="43" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/database/mysql.png" /> 
   <img width="40" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/database/postgres.png" />
  <img width="40" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/database/mongo.png" />
     <img width="42" height="44" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/database/firebase.png" />
  <img width="40" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/database/sqldeveloper.png" />
   <img width="40" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/database/dbeaver.png" />
     <img width="40" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/database/xampp.png" />
  
 
</div>

</br>

* Data Science Stack

<div style="display: inline-block;">
  <img width="40" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/python.png" />
  <img width="40" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/datascience/numpy.png" />
  <img width="38" height="35" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/datascience/pandas.png" />
    <img width="40" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/datascience/matplotlib.png" />
  <img width="42" height="44" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/datascience/pycharm.png" />
  <img width="40" height="42" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/datascience/jupyter.png" />
</div>


</br>

* Embedded System Stack

<div style="display: inline-block;">
   <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/cplusplus/cplusplus-original.svg" />
    <img width="45" height="45" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/embebed/raspberry.png" />
  <img width="40" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/embebed/arduino.png" />
    <img width="40" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/embebed/esp8266.png" />
  <img width="35" height="40" src="https://cdn-icons-png.flaticon.com/512/6080/6080697.png" />
  <img width="45" height="40" src="https://cdn.icon-icons.com/icons2/1381/PNG/512/fritzing_94885.png" />
  <img width="45" height="45" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/embebed/micropython.png" />
  <img width="45" height="45" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/embebed/microchip.png" />
   <img width="45" height="45" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/embebed/piccompiler.png" />
    <img width="45" height="45" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/embebed/thonny.png" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/linux/linux-original.svg" />
</div>

</br>




<!-----------------------------------------CANAL DE YOUTUBE------------------------------------------------------->

<!-- ## <img width="58" height="48"  src="https://github.com/andresWeitzel/Graphics/blob/main/Gifs/youtubeLoading.gif" /> Mi Canal <img width="58" height="48"  src="https://github.com/andresWeitzel/Graphics/blob/main/Gifs/youtubeLoading.gif" /> -->

---
## Mi Canal

</br>

<a href="https://www.youtube.com/channel/UCuSVXmBcMURyTvbmbcgZalQ?view_as=subscriber" target="_blank">
<img align="left" alt="andresWeitzel | YouTube" width="87" height="80" src="https://github.com/andresWeitzel/Graphics/blob/main/Gifs/youtubeLogo.gif" /></a> 

<a href="https://www.youtube.com/channel/UCuSVXmBcMURyTvbmbcgZalQ?view_as=subscriber" target="_blank">
 
 ***(PROGRAMACIÓN, ROBÓTICA, INFORMÁTICA Y MÁS..)*** ✔</a>  
 
<!-----------------------------------------FIN CANAL DE YOUTUBE------------------------------------------------------->

</br>

</br>

</br>



<!-----------------------------------------PROYECTOS------------------------------------------------------->

<!-- ## <img width="53" height="43"  src="https://github.com/andresWeitzel/Graphics/blob/main/Gifs/cube.gif" />Proyectos <img width="53" height="43" src="https://github.com/andresWeitzel/Graphics/blob/main/Gifs/cube.gif" /> -->
---
<div align="center">

## Proyectos y Desarrollos

</div>
 
 > Mis Proyectos, Códigos, Plantillas y Material los declaro OPEN SOURCE y Bajo [Licencia GNU GPL (GENERAL PUBLIC LICENSE)](https://www.gnu.org/licenses/gpl-3.0.html). 


<!--<<<<<<<<<<<<<<<<<<<<<<<<<<<<SECCIÓN WEB>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->

</br>

</br>


<div align="center">

## <img width="50" height="40"  src="https://github.com/andresWeitzel/Graphics/blob/master/Gifs/spheres/sphere01-unscreen.gif" /> Sitios Web <img width="50" height="40"  src="https://github.com/andresWeitzel/Graphics/blob/master/Gifs/spheres/sphere01-unscreen.gif" />

</div>  
  
  
</br>


<div align="center">
<p align="center">
  
 ### { Sitio Web acerca de Robótica desarrollado con HTML5, CSS3 y Boostrap V4.6}

  <img src="https://github.com/andresWeitzel/Graphics/blob/main/Proyectos/PaginaRobotica/Captura%20de%20pantalla%20(318).png" width="800" height="650" title="hover text">

 ###  Link : https://github.com/andresWeitzel/andresWeitzel.github.io
 ###  Tecnologías Empleadas 
  
</p>  
   
 <div style="display: inline-block;">
   <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/html5/html5-original.svg" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/css3/css3-original.svg" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/bootstrap/bootstrap-original.svg" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/vscode/vscode-original.svg" />
 </div>
 </div>
  
  
  </br>
  
  </br>
  
  </br>




<div align="center">
<p align="center">
  
### { Sitio Web para Tests en el Área IT  desarrollado con JS Vanilla, Boostrap v4.6 y Otras Tecnologías}

  <img src="https://github.com/andresWeitzel/Graphics/blob/main/Proyectos/SitioTestIt/Captura%20de%20pantalla%20(319).png" width="800" height="650" title="hover text">
 
 ###  Link : https://github.com/andresWeitzel/SitioWebTest_IT
 ###  Tecnologías Empleadas 
  
</p>  
 
 <div style="display: inline-block;">
   <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/html5/html5-original.svg" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/css3/css3-original.svg" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/bootstrap/bootstrap-original.svg" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1451/PNG/128/jsfolder_99356.png" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/vscode/vscode-original.svg" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2107/PNG/128/file_type_netlify_icon_130354.png" />
 
  </div>
 </div>
  
  
  
</br>
  
</br>

</br>

</br>
  
</br>




<!--APLICACIONES WEB-->

<div align="center">

## <img width="38" height="38"   src="https://github.com/andresWeitzel/Graphics/blob/master/Gifs/spheres/sphere02-unscreen.gif" /> Aplicaciones Web <img width="38" height="38"   src="https://github.com/andresWeitzel/Graphics/blob/master/Gifs/spheres/sphere02-unscreen.gif" />

</div>  



</br>

<div align="center">
<p align="center">
  
 ### { Aplicación Web para la Gestión de Productos de Microelectrónica implementando Spring Boot, Maven, Lombok, Thymeleaf, Bootstrap, Js , Api Highcharts, Open-Api-v3.0, Oracle y Otras Tecnologías }
 
  <img src="https://github.com/andresWeitzel/AppGestionMicroelectronica_SpringBoot/blob/master/documentation/inicio/inicioComponentes.png" width="800" height="650" title="hover text">

 ###  Link : https://github.com/andresWeitzel/AppGestionMicroelectronica_SpringBoot
 ###  Tecnologías Empleadas 
  
 </p>
  
 <div style="display: inline-block;">
   <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/html5/html5-original.svg" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/css3/css3-original.svg" />  
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/bootstrap/bootstrap-original.svg" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/javascript/javascript-original.svg" />
     <img width="45" height="45" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/java.png" />
  <img width="40" height="38" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/spring/spring-original.svg" />
  <img width="40" height="38" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/hibernate.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2107/PNG/512/file_type_maven_icon_130397.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2104/PNG/512/api_icon_129131.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/3053/PNG/512/postman_alt_macos_bigsur_icon_189814.png" />
  <img width="45" height="44" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/maven.png" />
  <img width="47" height="50" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/lombok.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2104/PNG/512/analytics_icon_129492.png" />
  <img width="44" height="44" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/sts.png" />
  <img width="44" height="44" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/git.png" />
   
 </div>
 </div>


 </br>
  
  </br>
  
  </br>


<div align="center">
<p align="center">
  
 ### { Aplicación Web para la Gestión de Productos IOT desarrollado con Java 8 EE, Maven, JSP, Servlets, BootstrapV4.6 y Otras Tecnologías }
  

  <img src="https://raw.githubusercontent.com/andresWeitzel/Graphics/main/Proyectos/IotProductosJsp_app/Captura%20de%20pantalla%20(317).png" width="800" height="650" title="hover text">
 
 ###  Link : https://github.com/andresWeitzel/IotProductosJsp_app
 ###  Tecnologías Empleadas
  
 </p>
 
  
 <div style="display: inline-block;">
   <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/html5/html5-original.svg" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/css3/css3-original.svg" />  
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/bootstrap/bootstrap-original.svg" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/java_original_wordmark_logo_icon_146459.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2107/PNG/512/file_type_maven_icon_130397.png" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/vscode/vscode-original.svg" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1381/PNG/512/netbeans_94416.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/mysql_original_wordmark_logo_icon_146417.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1381/PNG/512/xampp_94513.png" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" />
   
 </div>
 </div>
 
 
 </br>
  
  </br>
  
  </br>

<div align="center">
<p align="center">
  
 ### { Aplicación Web sobre Violencia de Género, Discriminación, etc desarrollado con Angular12, Boostrapv4.6, HTML5, CSS3 y Otras Tecnologías }
 
  <img src="https://raw.githubusercontent.com/andresWeitzel/Graphics/main/Proyectos/DenunciasOnlineAngular_app/Captura%20de%20pantalla%20(320).png" width="800" height="650" title="hover text">

 ### Link : https://github.com/andresWeitzel/WebAppAngularBootstrap
 ###  Tecnologías Empleadas 
  
 </p>
  
 <div style="display: inline-block;">
   <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/html5/html5-original.svg" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/css3/css3-original.svg" />  
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/bootstrap/bootstrap-original.svg" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/javascript/javascript-original.svg" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/angularjs/angularjs-original.svg" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/vscode/vscode-original.svg" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2107/PNG/128/file_type_netlify_icon_130354.png" />
   
 </div>
 </div>



  
  


<!--FIN APLICACIONES WEB-->

<!--<<<<<<<<<<<<<<<<<<<<<<<<<<FIN SECCIÓN WEB>>>>>>>>>>>>>>>>>>>>>>>>>>>-->

</br>
  
</br>

</br>

</br>
  
</br>



<!--<<<<<<<<<<<<<<<<<<<<<<<<<<<SECCIÓN DESKTOP APP>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->

<div align="center">

## <img width="38" height="38"   src="https://github.com/andresWeitzel/Graphics/blob/master/Gifs/spheres/sphere06-unscreen.gif" /> Aplicaciones de Escritorio  <img width="38" height="38"   src="https://github.com/andresWeitzel/Graphics/blob/master/Gifs/spheres/sphere06-unscreen.gif" />

</div>  



</br>

<div align="center">
<p align="center">

 ### { Aplicación de Escritorio Java 8 SE para la Gestión de Empleados }

  <img src="https://raw.githubusercontent.com/andresWeitzel/Graphics/main/Proyectos/GestorEmpleados_app/Captura%20de%20pantalla%20(341).png" width="800" height="650" title="hover text">
 
 ###  Link : https://github.com/andresWeitzel/Gestor_de_Empleados
 ###  Tecnologías Empleadas
  
  
</p>

 <div style="display: inline-block;">
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/java_original_wordmark_logo_icon_146459.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2107/PNG/512/file_type_maven_icon_130397.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1381/PNG/512/netbeans_94416.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/mysql_original_wordmark_logo_icon_146417.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1381/PNG/512/xampp_94513.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2104/PNG/512/analytics_icon_129492.png" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" />
 
  </div>
  </div>
  
  
  
 </br>
  
 </br>
 
 </br>
 
 
 

<div align="center">
<p align="center">
  
 ### { Aplicación de Escritorio Java 8 SE para el Control de Fármacos en Chimpancés }

  <img src="https://github.com/andresWeitzel/Graphics/blob/main/Proyectos/FarmacoNTZ184/Captura%20de%20pantalla%20(515).png" width="800" height="650" title="hover text">
  
 ### Link : https://github.com/andresWeitzel/Farmaco_NTZ184
 ### Tecnologías Empleadas 
  
</p>


 <div style="display: inline-block;">
 <div style="display: inline-block;">
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/java_original_wordmark_logo_icon_146459.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2107/PNG/512/file_type_maven_icon_130397.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1381/PNG/512/netbeans_94416.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/mysql_original_wordmark_logo_icon_146417.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1381/PNG/512/xampp_94513.png" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" />
 </div>
 </div>
  
  
  
  </br>

  </br>
  
 </br>
 
 
 

<div align="center">
<p align="center">
  
### { Aplicación de Escritorio Java 8 SE para la Gestión de Gastos Personales  }

  <img src="https://github.com/andresWeitzel/Graphics/blob/master/Proyectos/GestorGastosPersonales_app/Captura%20de%20pantalla%20(516).png" width="800" height="650" title="hover text">

 ### Link : https://github.com/andresWeitzel/Gestor_Gastos_Personales
 ###  Tecnologías Empleadas 

</p>
  
 <div style="display: inline-block;">
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/java_original_wordmark_logo_icon_146459.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2107/PNG/512/file_type_maven_icon_130397.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1381/PNG/512/netbeans_94416.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/mysql_original_wordmark_logo_icon_146417.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1381/PNG/512/xampp_94513.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2104/PNG/512/analytics_icon_129492.png" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" />
 
  </div>
 </div>
  

<!--<<<<<<<<<<<<<<<<<<<<<<<<<<<FIN SECCIÓN DESKTOP APP>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->




</br>
  
</br>

</br>

</br>
  
</br>


<!--<<<<<<<<<<<<<<<<<<<<<<<<<<<SECCIÓN API-REST MICROSERVICES>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->

<div align = "center">

  ##  <img width="47" height="47" src="https://github.com/andresWeitzel/Graphics/blob/master/Gifs/spheres/sphere03-unscreen.gif" />  Api-Rest / Microservices  <img width="47" height="47" src="https://github.com/andresWeitzel/Graphics/blob/master/Gifs/spheres/sphere03-unscreen.gif" />

</div>

</br>

 
<div align="center">
<p align="center">

 ### { Microservicios con Spring Boot, Spring Cloud Eureka, Grafana, Prometheus, ApiGateway, Spring Data JPA, Resilience4J, Lombok, Maven, Postman, Postgres, Mysql y otras Tecnologías }

  <img src="https://github.com/andresWeitzel/Microservicios_Spring_Cloud_Netflix_Spring_Boot/blob/master/documentacion/MicroservicesArquitectura.png" width="800" height="650"  title="hover text">

   ###  Link : https://github.com/andresWeitzel/Microservicios_Spring_Cloud_Netflix_Spring_Boot
   ### Tecnologías Empleadas 
  
 </p>


 <div style="display: inline-block;">
  <img width="45" height="45" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/java.png" />
  <img width="40" height="38" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/spring/spring-original.svg" />
  <img width="40" height="38" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/hibernate.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2107/PNG/512/file_type_maven_icon_130397.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2104/PNG/512/api_icon_129131.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/3053/PNG/512/postman_alt_macos_bigsur_icon_189814.png" />
  <img width="45" height="44" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/maven.png" />
  <img width="47" height="50" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/lombok.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2104/PNG/512/analytics_icon_129492.png" />
  <img width="44" height="44" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/sts.png" />
  <img width="44" height="44" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/git.png" />
  </div>
  </div>


 </br>
  
  </br>
  
  </br>

 
<div align="center">
<p align="center">

 ### { Desarrollo de una Api Rest con Spring Boot, SpringDoc-OpenApi, Swagger UI, Maven, Lombok, Log4j y Oracle sobre Componentes para Microelectrónica }

  <img src="https://github.com/andresWeitzel/ApiRest_Microelectronica_SpringBoot_Oracle/blob/master/documentation/swagger/doc_openapi_componenteController/listado_componentes.png" width="800" height="650"  title="hover text">

   ###  Link : https://github.com/andresWeitzel/ApiRest_Microelectronica_SpringBoot_Oracle
   ### Tecnologías Empleadas 
  
 </p>


 <div style="display: inline-block;">
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/java_original_wordmark_logo_icon_146459.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2107/PNG/512/file_type_maven_icon_130397.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/tomcat_original_wordmark_logo_icon_146324.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1250/PNG/512/1494258020-leafspringplantecologygreen_84346.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2107/PNG/512/file_type_swagger_icon_130134.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2104/PNG/512/api_icon_129131.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/3053/PNG/512/postman_alt_macos_bigsur_icon_189814.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/9/PNG/256/sql_racer_gamedatabase_sql_1526.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/oracle_original_logo_icon_146401.png" />  
    <img width="47" height="50" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/lombok.png" />
  <img width="44" height="44" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/sts.png" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" />
  </div>
  </div>




 </br>
  
  </br>
  
  </br>

 
<div align="center">
<p align="center">

 ### { Api Rest acerca de Productos Electrónicos con Spring Boot, Spring Data MongoDB, SpringDoc-OpenApi, Swagger UI, Maven, Lombok, Log4j, Git y MongoDB }

  <img src="https://github.com/andresWeitzel/ApiRest_ElectroThingsV1_SpringBoot_MongoDB/blob/master/documentation/productosController.png" width="800" height="650"  title="hover text">

   ###  Link : https://github.com/andresWeitzel/ApiRest_ElectroThingsV1_SpringBoot_MongoDB
   ### Tecnologías Empleadas 
  
 </p>


 <div style="display: inline-block;">
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/java_original_wordmark_logo_icon_146459.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2107/PNG/512/file_type_maven_icon_130397.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/tomcat_original_wordmark_logo_icon_146324.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1250/PNG/512/1494258020-leafspringplantecologygreen_84346.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2107/PNG/512/file_type_swagger_icon_130134.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2104/PNG/512/api_icon_129131.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/3053/PNG/512/mongodb_compass_macos_bigsur_icon_189933.png" />
 <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/mongodb_plain_wordmark_logo_icon_146423.png" />
    <img width="47" height="50" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/lombok.png" />
  <img width="44" height="44" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/back/sts.png" />
 <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/92/PNG/256/cmd_16549.png" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" />
  </div>
  </div>




 </br>
  
  </br>
  
  </br>


 
<div align="center">
<p align="center">

 ### { Desarrollo de una Api Rest con Spring Boot, Spring Security, JWT, etc sobre Productos Electrónicos  }

  <img src="https://github.com/andresWeitzel/Graphics/blob/master/Proyectos/ApiRestProductosSpringBoot/CRUD/delete/copyListado.jpg" width="800" height="650"  title="hover text">

   ###  Link : https://github.com/andresWeitzel/Api_Rest_Spring_Productos
   ### Tecnologías Empleadas 
  
 </p>


 <div style="display: inline-block;">
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/java_original_wordmark_logo_icon_146459.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2107/PNG/512/file_type_maven_icon_130397.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1381/PNG/512/eclipse_94656.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/tomcat_original_wordmark_logo_icon_146324.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1250/PNG/512/1494258020-leafspringplantecologygreen_84346.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1512/PNG/512/40_104848.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2790/PNG/512/json_filetype_icon_177531.png" />
   <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/3053/PNG/512/postman_alt_macos_bigsur_icon_189814.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/mysql_original_wordmark_logo_icon_146417.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1381/PNG/512/xampp_94513.png" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" />
  </div>
  </div>
 



 </br>
  
  </br>
  
  </br>

 
<div align="center">
<p align="center">

 ### {  Colaboración en un Proyecto sobre el Covid-19 Área Core/Api-Rest con PHP  }

  <img src="https://raw.githubusercontent.com/andresWeitzel/Graphics/master/Proyectos/MedMask/Captura%20de%20pantalla%20(518).png" width="800" height="650"  title="hover text">

   ###  Link : https://github.com/andresWeitzel/medmask
   ### Tecnologías Empleadas 
  
 </p>


 <div style="display: inline-block;">
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2108/PNG/512/php_icon_130857.png" />
    <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/java_original_wordmark_logo_icon_146459.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2104/PNG/512/api_icon_129131.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1381/PNG/512/netbeans_94416.png" />
 <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/mysql_original_wordmark_logo_icon_146417.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1381/PNG/512/xampp_94513.png" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" />
 
  </div>
  </div>
 



<!--<<<<<<<<<<<<<<<<<<<<<<<<<<<FIN SECCIÓN API-REST MICROSERVICES>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->

</br>
  
</br>

</br>

</br>
  
</br>


<!--<<<<<<<<<<<<<<<<<<<<<<<<<<<SECCIÓN DATABASES>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->

<div align = "center">

##  <img width="45" height="40"   src="https://github.com/andresWeitzel/Graphics/blob/master/Gifs/spheres/sphere05-unscreen.gif" /> Bases de Datos <img width="45" height="40"   src="https://github.com/andresWeitzel/Graphics/blob/master/Gifs/spheres/sphere05-unscreen.gif" /> 
 <div >


 </br>
  
  
  <div align="center">
<p align="center">

 ### { Diseño, Programación y Administración de una DB de Inmobiliaria con PostgreSQL }

  <img src="https://github.com/andresWeitzel/Graphics/blob/master/Proyectos/db_Inmobiliaria/Captura%20de%20pantalla%20(526).png" width="800" height="650"  title="hover text">

  ### Link : https://github.com/andresWeitzel/db_Inmobiliaria_PostgreSQL
  ### Tecnologías Empleadas 
  
  </p>


 <div style="display: inline-block;">
  
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1495/PNG/512/dbeaver_103190.png" />
 <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/postgresql_original_wordmark_logo_icon_146392.png" />
 <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/92/PNG/256/cmd_16549.png" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" />
 
  </div>
  </div>

  

 </br>
  
  </br>
  
  </br>
  

  
  
  <div align="center">
<p align="center">

 ### { MicroDB Inmobiliaria para el Consumo de MicroServicios con PostgreSQL }

  <img src="https://github.com/andresWeitzel/Microdb_productos_supermercado_PostgreSQL/blob/master/documentation/listado-productos.png" width="800" height="350"  title="hover text">

  ### Link : https://github.com/andresWeitzel/db_inmobiliaria_microservicios_postgres
  ### Tecnologías Empleadas 
  
  </p>


 <div style="display: inline-block;">
  
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1495/PNG/512/dbeaver_103190.png" />
 <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/postgresql_original_wordmark_logo_icon_146392.png" />
 <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/92/PNG/256/cmd_16549.png" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" />
 
  </div>
  </div>

  

 </br>
  
  </br>
  
  </br>
   
   
   
   <div align="center">
<p align="center">

 ### { MicroDB acerca de Productos Electrónicos con MongoDB }

  <img src="https://github.com/andresWeitzel/db_ElectroThings_MongoDB/blob/master/doc/collection_productos/collection.json.png" width="800" height="650"  title="hover text">

  ### Link : https://github.com/andresWeitzel/db_ElectroThings_MongoDB
  ### Tecnologías Empleadas 
  
  </p>


 <div style="display: inline-block;">
  
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/3053/PNG/512/mongodb_compass_macos_bigsur_icon_189933.png" />
 <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/mongodb_plain_wordmark_logo_icon_146423.png" />
 <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/92/PNG/256/cmd_16549.png" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" />
 
  </div>
  </div>

  

   </br>
  
  </br>
  
  </br>
  
  
  <div align="center">
<p align="center">

 ### { Base de Datos acerca de un Supermercado con PostgreSQL }

  <img src="https://github.com/andresWeitzel/db_supermercado/blob/master/doc/db_supermercado_ER.png" width="800" height="650"  title="hover text">

  ### Link : https://github.com/andresWeitzel/db_supermercado
  ### Tecnologías Empleadas 
  
  </p>


 <div style="display: inline-block;">
  
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1495/PNG/512/dbeaver_103190.png" />
 <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/postgresql_original_wordmark_logo_icon_146392.png" />
 <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/92/PNG/256/cmd_16549.png" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" />
 
  </div>
  </div>

  
  
   </br>
  
  </br>
  
  </br>
  
 
  
  <div align="center">
<p align="center">

 ### { Base de Datos acerca de Productos de Microelectrónica con Oracle }

  <img src="https://github.com/andresWeitzel/db_microelectronica_Oracle/blob/master/doc/db_microelectronica_DER.png" width="800" height="650"  title="hover text">

  ### Link : https://github.com/andresWeitzel/db_microelectronica_Oracle
  ### Tecnologías Empleadas 
  
  </p>


 <div style="display: inline-block;">
  
  <img width="40" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/database/sqldeveloper.png" />
 <img width="40" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/GithubReadme/database/oracle.png" />
 <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/92/PNG/256/cmd_16549.png" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" />
 
  </div>
  </div>

  
  
  </br>
  
  </br>
  
  </br>
  
 
<div align="center">
<p align="center">

 ### {  Modelado, Diseño y Gestión de una DB de Indumentaria con Mysql }

  <img src="https://raw.githubusercontent.com/andresWeitzel/Gestion_BaseDeDatos_Mysql/master/documentation/06_logsUpdate.png" width="800" height="650"  title="hover text">

  ### Link : https://github.com/andresWeitzel/Gestion_BaseDeDatos_Mysql
  ### Tecnologías Empleadas 
  
  </p>


 <div style="display: inline-block;">
  
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1495/PNG/512/dbeaver_103190.png" />
 <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/mysql_original_wordmark_logo_icon_146417.png" />
 <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1381/PNG/512/xampp_94513.png" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" />
 
  </div>
  </div>
 



<!--<<<<<<<<<<<<<<<<<<<<<<<<<<<FIN SECCIÓN DATABASES>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->

</br>
  
</br>

</br>

</br>
  
</br>
  



<!--<<<<<<<<<<<<<<<<<<<<<<<<<<<SECCIÓN EMBEDDED SYSTEMS>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->

<div align="center">
  
## <img width="40" height="40"  src="https://github.com/andresWeitzel/Graphics/blob/master/Gifs/spheres/sphere11-unscreen.gif" />  Sistemas Embebidos  <img width="40" height="40" src="https://github.com/andresWeitzel/Graphics/blob/master/Gifs/spheres/sphere11-unscreen.gif" /> 

</div>

 
 
 </br>
 
 
<div align="center">
<p align="center">
 
 ### {  Sistema de Control de Nivel de Agua con Arduino Mega y Sensores  }
  
  <img src="https://github.com/andresWeitzel/Graphics/blob/master/Proyectos/SistemaControlTanqueAgua/internal.png" width="800" height="650"  title="hover text">

 ### Link : https://github.com/andresWeitzel/Proyecto-Sistema-de-Control-para-Tanque-de-Agua
 ### Tecnologías Empleadas 
  
  </p>

 <div style="display: inline-block;">
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/159/PNG/256/arduino_22429.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/7/PNG/128/mimetypes_cppsource_c_341.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1381/PNG/512/fritzing_94885.png" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" />
 
  </div>
  </div>
 
 
 
  </br>
  
  </br>
  
  </br>

 
<div align="center">
<p align="center">

 ### {  Sistema de Sensado de Temp/Hum con el Micro ESP8266 y DHT11  }

  <img src="https://github.com/andresWeitzel/Graphics/blob/master/Proyectos/Sensado_ESP8266/Captura%20de%20pantalla%20(517).png" width="800" height="650"  title="hover text">

  
 ###  Link : https://github.com/andresWeitzel/Sensado_ESP8266_DHT11
 ###  Tecnologías Empleadas 
  
 </p>

 <div style="display: inline-block;">
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/159/PNG/256/arduino_22429.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/7/PNG/128/mimetypes_cppsource_c_341.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/3053/PNG/512/nodemcu_pyflasher_macos_bigsur_icon_189912.png" />
  <img width="40" height="40" src="https://cdn-icons-png.flaticon.com/512/6080/6080697.png" />
  <img width="40" height="40" src="https://cdn.icon-icons.com/icons2/1381/PNG/512/fritzing_94885.png" />
  <img width="40" height="40" src="https://cdn-icons-png.flaticon.com/512/3175/3175966.png" />
  <img width="40" height="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" />
 
  </div>
 </div>
 


<!--<<<<<<<<<<<<<<<<<<<<<<<<<<<FIN SECCIÓN EMBEDDED SYSTEMS>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->

</br>
</br>
</br>

 > He desarrollado en mayor o menor medida otros 27 proyectos de los cuáles 20 están incompletos/sin revisión y los otros 7 son proyectos a "gran escala personal" que están activos y en proceso de desarrollo, por ende, ambos al no ser funcionalmente aceptables no los incluyo. Algunos de ellos son:
 > *  Una Interfaz Gráfica de Comunicación entre un microcontrolador a través de http y wifi y una db, aplicando java, html5, css3, js vanilla, mysql, scripting y c++ .
 > *  Aplicación para recolección de mediciones de un sensor en tiempo real con un esp8266 y un módulo sensor infrarrojo
 > *  Desarrollo y Programación de un circuito electrónico para mediciones eléctricas de mi casa para digitalización y procesamiento usando un Arduino, varios módulos, C++, python, PHP y mysql como db.
 > * Sistema de Riego Automático para mi Plantas y Relevamiento de datos (Hardware y Software)
 > * Sistema de Control de Temperatura para mi Habitación (Automatización de Ventilador y Estufa) con almacenamiento de datos
 > * Versionador de Software usando Spring Boot, Java NIO, Hibernate y Oracle
 > * Interfaz Gráfica tipo Dashboard implementando Python, Angular, Js Vanilla
 > * Migración de un Proyecto de Gestión desarrollado con Java 8 SE migrando a Hibernate-JPA.
 > * Aplicación de Escritorio para la gestión de tareas personales
 > * Otros
 
 > Los otros 5 proyectos a "gran escala personal" son : 
 > * Etapa de diseño de un Sistema Operativo Híbrido (no fork) con características similares de Windows y Linux. (Diseño y planificación de DD(device drivers) para compatibilidad de proveedores, Análisis de Hibridación de partes del Core de Windows Core, Diseño de una Shell con generación de utilidades automáticas en tiempo de uso).
 > * Planificación de algoritmos (estilos bots) que ayuden a la codificación en paralelo ( idea planificada : Generación Automática de procedimientos almacenados y funciones en base de datos, tarea repetitiva y escalabilidad alta de código. Gran margen de error, revisión de código semi automatizada  )
 > * Aplicación Generadora de Contextualización Física (objetos, secuencias, ubicaciones) en base a una idea como parámetro de entrada para varios algoritmos de IA y Redes Neuronales (etapa de planificación general). La idea surgió en base a poder utilizar toda la potencia de renderizado actual de una cpu/gpu medianamente convencional
 > * Sistema de monitorización de presión y oxigenación sanguínea(estilo oxímetro) para prevenir insuficiencias cardiacas aplicando algoritmos de IA tanto para la manipulación de muestreos en vista dashboard en tiempo de recopilación  y manipulación de datos desde diferentes bases de datos (estilo microservicios) que almacenen las actividades, localizaciones y otras ideas en relación a la muestra obtenida, generación de reportes programados y gestión de revisión automática por algoritmos de detección según patrones médicos ingresados.
> * Otros privados

<!-----------------------------------------FIN PROYECTOS------------------------------------------------------->
